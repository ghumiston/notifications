// - rest/notification/inbox/status
export const updateNotificationStatus = (requestPayload) => fetch('http://localhost:3000/services/notification/inbox/status.json', 
    {
        method:'PUT',
        body: JSON.stringify(requestPayload)
    }
);

// - rest/notification/addWithRecipList
export const addWithRecipList = (requestPayload, headerInfo = {}) => {
    
    const { xxsid } = headerInfo;
    const headers = {
        'xe-xxsid': xxsid
    };

    fetch('http://localhost:3000/services/notification/addWithRecipList.json', 
    {
        method:'POST',
        headers,
        body: JSON.stringify(requestPayload)
    }
  );
};

// TODO: review these endpoints with Brian in relation
// to how stubbed data should sit (GCH)

// - rest/notification/inbox/flag
// requestPayload: { IsFlagged: true, ItemID: [1200, 1205]}
export const updateNotificationFlagStatus = (requestPayload) => fetch(
  'http://localhost:3000/services/notification/inbox/flag.json',
  {
    method: 'PUT',
    body: requestPayload
  }
);

// Endpoints hit during a notification deletion request
// - rest/notification/inbox/display
// requestPayload: { IsDisplayed: false, ItemID: [1201, 1205] }

// followed by....
// GET rest/notification/inbox/1201/itemDetails
// GET rest/notification/thrasys/inbox?category=m&isUnconfirmedOnly=false

// TODO: what is the point of these requests? They occur after a brief loading screen
// following a notification deletion (GCH)
// PUT rest/notification/inbox/status - {ItemID: [1205], StatusID: "READ"}
// GET rest/notification/inbox/1205/itemDetails
// GET rest/notification/thrasys/inbox?category=m&isUnconfirmedOnly=false
export const deleteNotification = (requestPayload) => fetch(
  'http://localhost:3000/services/notification/inbox/display.json',
  {
    method: 'PUT',
    body: requestPayload
  }
);
