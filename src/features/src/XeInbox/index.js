import React from 'react';

import NotificationList from './components/NotificationList';
import NotificationDetails from './components/NotificationDetails';
import { useInboxQuery, useParseIdParam } from './hooks';
import ErrorView from './components/ErrorView';
import LoadingView from './components/LoadingView';

export const XeInbox = () => {
    const { XeAppUserNotifications = [], isLoading, isError, error } = useInboxQuery();
    const selectedNotificationId = useParseIdParam();

    return isLoading 
      ? <LoadingView/>
      : isError
      ? <ErrorView errorMessage={error.message}/>
      : <div>
          <h1>XeInbox</h1>
          <NotificationList
            xeAppUserNotifications={XeAppUserNotifications}
            selectedNotificationId={selectedNotificationId}
          />
          <NotificationDetails selectedNotificationId={selectedNotificationId}/>
      </div>;
};

export default XeInbox;