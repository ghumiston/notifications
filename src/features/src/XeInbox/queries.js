// - rest/notification/thrasys/inbox?category=m&isUnconfirmedOnly=false
export const toInboxQuery = () => () => 
  fetch('http://localhost:3000/services/notification/thrasys/inbox.json').then(res =>
    res.json()
  );

// - rest/notification/inbox/1202/itemDetails
export const toItemDetailsQuery = (selectedNotificationId) => () =>
  fetch(`http://localhost:3000/services/notification/${selectedNotificationId}/itemDetails.json`).then(res =>
      res.json()
  );

  // - rest/patient/getPatient?ipid=166238
  export const toGetPatientQuery = (ipid) => () => {
    if (!ipid) {
      return Promise.resolve(undefined)
    }
    return fetch(`http://localhost:3000/services/patient/getPatient.json?ipid=${ipid}`).then(res =>
        res.json()
    );
  }

  