import React from 'react';
import './ActionBar.css';

const ActionButton = ({ 
  onClick = () => {}, 
  label = '', 
  disabled = false 
}) => {

  return (
    <button
      className={
        `k-button 
        k-outline 
        ${disabled ? 'k-button-disabled' : ''} 
        xe-button 
        xjs-icon-button 
        xjs-icon-button--flag 
        icon-button`
      }
      onClick={!disabled ? onClick : () => {}}
    >
    {label}
    </button>
  );
}

export const ActionBar = ({disabled}) => {
  return (
    <div className="flag-bar flex-container align-items-center">
      <ActionButton
        label="Flag"
        disabled={disabled}
        onClick={() => {}}
      />
      <ActionButton
        label="Mark as Unread"
        disabled={disabled}
        onClick={() => {}}
      />
      <ActionButton
        label="Delete"
        disabled={disabled}
        onClick={() => {}}
      />
      {/* Note: refresh kicks off a GET from the inbox endpoint.
        Maybe we can invalidate the inbox query to get a more 
        up-to-date notifications list? (GCH)
      */}
      <ActionButton
        label="Refresh"
        onClick={() => {}}
      />
    </div>
  );
};

export default ActionBar;