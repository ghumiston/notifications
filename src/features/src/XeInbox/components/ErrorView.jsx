import React from 'react';

export const ErrorView = ({ errorMessage }) => (
  <div class="error-screen">
    <span>{`Error loading notifications: ${errorMessage}`}</span>
  </div>
);

export default ErrorView;