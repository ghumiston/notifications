import React/*, { useEffect }*/ from 'react';
import NotificationHeader from './notificationDetails/NotificationHeader';
import NotificationBody from './notificationDetails/NotificationBody';
import NotificationReply from './notificationDetails/NotificationReply';
import { useItemDetailsQuery, useGetAssociatedPatient } from '../hooks';
//import {updateNotificationStatus} from '../mutations';

//import { useMutation } from 'react-query';
// related endpoints
// - rest/notification/inbox/status
// - rest/notification/inbox/1202/itemDetails

const NotificationDetails = ({ selectedNotificationId }) => {
  
  const currentXeAppUserNotification = useItemDetailsQuery(selectedNotificationId);
  const associatedPatient = useGetAssociatedPatient(currentXeAppUserNotification)

/*
  This code will use a mutation to set the Status id of the current notification in the case 
  that it was not read, then was renderered on teh screen.
  const { mutate } = useMutation(updateNotificationStatus);
  useEffect(() => {
    const { StatusID, NotificationID } = currentXeAppUserNotification;
    if(StatusID === 'UNREAD') {
      mutate({ItemID: [NotificationID], StatusID: 'READ'});
      console.log("Mark As Read");
    }
  })
*/

//TODO: What is this
const canReply = true;

  return (
      <div>
        <h1>{`Notification Details`}</h1>
          {
            currentXeAppUserNotification && 
            <div>
              <NotificationHeader 
                xeAppUserNotification={currentXeAppUserNotification}
                associatedPatient={associatedPatient}
              />
              <NotificationBody xeAppUserNotification={currentXeAppUserNotification}/>
              { canReply && 
                <NotificationReply 
                  xeAppUserNotification={currentXeAppUserNotification}
                  associatedPatient={associatedPatient}
                />
              }
            </div>
          }
      </div>
  );
};

export default NotificationDetails;