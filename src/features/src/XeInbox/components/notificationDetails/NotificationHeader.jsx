import React from 'react';
import Recipient from './notificationHeader/Recipient';
import {patientDetailsFormatter, visitFormatter} from '../../dataFormaters'

const COMMA = ', ';

//TODO: need to replace this when we move this project over.
const onPatientClick = () => {};
/*onPatientClick={() =>
  dispatch({
    type: SHOULD_NAVIGATE_TO_PATIENT,
    value: associatedPatient,
  })
}*/

export const NotificationHeader = ({
    xeAppUserNotification: { 
        Subject, 
        SendDate,
        SenderResID: {GivenName, FamilyName} = {},
        DerivedRecipientResID = []
     },
     associatedPatient}) => {

    //TODO: use pluck when we move everything over
    //const toVisitID = pluck('DefaultSelectedIVID', 'VisitID');
    const associatedVisitID = associatedPatient?.DefaultSelectedIVID?.VisitID;

    return (
      //TODO:  style this header when we move the project over
      <div className="notification-details__information margin-horizontal-medium">

          <div className="notification-details__subject-line padding-vertical-medium">
              <span className="subject-line__subject bold font-large padding-right-small">
                  {Subject}
              </span>
              <span className="notification-details__subject-line-label">
                  {SendDate}
              </span>
          </div>
          <div>
              <span className="margin-right-small">From:</span>
              <span>{GivenName} {FamilyName}</span>
          </div>
          <div>
              <span className="margin-right-small">To:</span>
              {DerivedRecipientResID.map((derivedRecipientResID, index, array) => 
                  <Recipient 
                    key={derivedRecipientResID.ResourceID}
                    derivedRecipientResID={derivedRecipientResID} 
                    showDelimiter={index !== array.length - 1} 
                    delimiter={COMMA}
                  />
              )}
          </div>
          <div>
            { associatedPatient && 
                (
                  <div>
                    <div className="margin-right-small">Patient:</div>
                    <span className="associated-patient" onClick={onPatientClick}>
                      {patientDetailsFormatter(associatedPatient)}
                    </span>
                  </div>
              )
            }
          </div>
          <div>
            {
              associatedVisitID && (
                <div>
                  Visit 
                  {visitFormatter(associatedPatient.DefaultSelectedIVID)}
                </div>
              )
            }
          </div>
      </div>)
};

export default NotificationHeader;