import React from 'react';

export const Recipient = (props) => {
    const {derivedRecipientResID: { FamilyName, GivenName} = {}, 
    delimiter, showDelimiter } = props;
  
    return (
      <span className={'name'}>
        {GivenName} {FamilyName} {showDelimiter && delimiter}
      </span>
    );
};

export default Recipient;