import React, { useState } from 'react';
import { useMutation } from 'react-query';
import ReplyHeader from './notificationReply/ReplyHeader';
import Attachments from './notificationReply/Attachments';
import ReplyFooter from './notificationReply/ReplyFooter';
import { addWithRecipList } from './../../mutations';  

//TODO:  see if we can clean this up at some point
const toAttachments = attachments =>
  attachments.length
    ? attachments.map(({ Data, FileName }) => ({
        Data,
        FileName,
        MimeType: 'file',
      }))
    : undefined;

  //TODO:  see if we can clean this up at some point
  const generateAddWithRecipListPayload = (
    {
      replyAll,
      replyMessage,
      replyAttachments,
      associatedPatient: { IPID, DefaultSelectedIVID = {} } = {},
      notification: {
        PriorityID: { PriorityID } = {},
        SenderResID = {},
        DerivedRecipientResID = [],
        Subject,
      } = {},
      userData: { XeResource = {} } = {}
    }
  ) =>
    {
      return Object.assign(
        {
          Attachment: toAttachments(replyAttachments),
          Body: replyMessage,
          IsUsername: false,
          // Just using SIMPLE_NOTIFICATION for now. This may become a conditional (JCM)
          NotificationTypeID: 'SIMPLE_NOTIFICATION',
          PriorityID,
          RecipientList: replyAll
            ? [SenderResID, ...DerivedRecipientResID]
                .map(({ ResourceID } = {}) => ResourceID)
                .filter(id => id !== XeResource.ResourceID)
                .join('|')
            : SenderResID.ResourceID,
            
          Subject: 'RE: ' + Subject,
        },
        IPID && { IPID },
        DefaultSelectedIVID.IVID && { IVID: DefaultSelectedIVID.IVID }
      );
    }
    


const toDiscardCallbackFn = (setReplyMessage) => () => setReplyMessage('');
const toOnTextAreaChangedFn = (setReplyMessage) => ({target: { value }}) => setReplyMessage(value);

const toCallAddWithRecipListFn = (mutate) => (payload, formatter) => () => mutate(formatter(payload));

const buildReplyPayload = (basePayload) => generateAddWithRecipListPayload({...basePayload, replyAll: false})
const buildReplyAllPayload = (basePayload) => generateAddWithRecipListPayload({...basePayload, replyAll: true})

const doesNotificationRequireConfirmation = (xeAppUserNotification) => {
  const {NotificationTypeID : { NotificationTypeID }, IsCompleted} = xeAppUserNotification;
  return NotificationTypeID === 'CONFIRMED_NOTIFICATION' && !IsCompleted;
}

const doesNotificationHaveMultipleRecipients = (xeAppUserNotification) => {
  const { DerivedRecipientResID = [] } = xeAppUserNotification;
  return DerivedRecipientResID.length > 1;
}


export const NotificationReply = ({xeAppUserNotification, userData, associatedPatient}) => {

    const { SenderResID, Attachments: attachments} = xeAppUserNotification;
    const [ replyMessage, setReplyMessage ] = useState();
    const { mutate } = useMutation(addWithRecipList);
  
    const notificationRequiresConfirmation = doesNotificationRequireConfirmation(xeAppUserNotification);
    const multipleRecipients = doesNotificationHaveMultipleRecipients(xeAppUserNotification);

    //TODO: fill this in when we have attachments
    const replyAttachments = [];    
  
    const basePayload = {
      replyMessage,
      replyAttachments,
      associatedPatient,
      notification: xeAppUserNotification,
      userData
    }
 
    // refactor these to pass in a basepayload and a formatter function so we dont build the object until we make the request
    const toCallAddWithRecipList = toCallAddWithRecipListFn(mutate);
    const replyCallback = toCallAddWithRecipList(basePayload, buildReplyPayload)
    const replyAllCallback = toCallAddWithRecipList(basePayload, buildReplyAllPayload)
    const discardCallback = toDiscardCallbackFn(setReplyMessage);
    const onTextAreaChanged = toOnTextAreaChangedFn(setReplyMessage);
    
    return  (
      <div>
          <ReplyHeader recipientLastName={SenderResID.FamilyName} recipientFirstName={SenderResID.GivenName} />
          <div><textarea onChange={onTextAreaChanged} value={replyMessage}></textarea></div>
          <Attachments attachments={attachments}/> 
          <ReplyFooter 
            replyCallback={replyCallback}
            replyAllCallback={replyAllCallback}
            discardCallback={discardCallback}
            notificationRequiresConfirmation={notificationRequiresConfirmation}
            multipleRecipients={multipleRecipients}
          />
      </div>
    )
};

export default NotificationReply;