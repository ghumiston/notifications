import React from 'react';

export const NotificationBody = ({xeAppUserNotification}) => {
    const { 
        Body, 
        NotificationTypeID, 
        CompletedDate, 
        StatusMessage 
    } = xeAppUserNotification;

    return (
        <div className="message-read-box margin-all-medium overflow-auto">
        <div className="notification-details__body">{Body}</div>
        {NotificationTypeID === 'CONFIRMED_NOTIFICATION' && NotificationTypeID && (
          <div className="red confirmed padding-top-medium">
            {'Confirmed: '}
            {CompletedDate}{' '}
            {StatusMessage}
          </div>
        )}
      </div>
    )
}

export default NotificationBody;