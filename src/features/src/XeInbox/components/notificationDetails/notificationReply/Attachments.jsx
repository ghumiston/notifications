import React from 'react';

export const Attachments = ({attachments}) => {
    if (!Array.isArray(attachments)) return null;
    
    return (
        <div className="attachments">
        {attachments.map(({ FileName, FileSize, FileID } = {}) => (
            <div
            key={FileID}
            className="attachment inline-flex-container align-items-center margin-vertical-medium margin-left-medium margin-right-large"
            >
            {/**<Icon icon={ATTACH_FILE} className="system-icon" />*/}
            {/*<span
                className="attachment-link"
                {onClick={() =>
                isFunction(onSelect) && onSelect({ FileID, FileName })
                }}
            >*/}
            <span
                className="attachment-link"
            >
                {FileName}
            </span>
            {/*<span className="grey">{fileSizeFormatter(parseInt(FileSize))}</span>*/}
            <span className="grey">{parseInt(FileSize)}</span>
            </div>
        ))}
        </div>
    );
};

export default Attachments