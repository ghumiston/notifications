import React from 'react';


const noop = () => {};

const shouldDisplayReplyToAll = (notificationRequiresConfirmation, multipleRecipients) => 
  !notificationRequiresConfirmation && multipleRecipients;

const getReplyButtonText = (notificationRequiresConfirmation) => 
  notificationRequiresConfirmation ? 'Confirm' : 'Reply';

export const ReplyFooter = ({ 
  replyCallback = noop, 
  replyAllCallback = noop,
  discardCallback = noop,
  notificationRequiresConfirmation = false,
  multipleRecipients = false
}) => {

    const displayReplyToAll = shouldDisplayReplyToAll(notificationRequiresConfirmation, multipleRecipients);
    const replyButtonText = getReplyButtonText(notificationRequiresConfirmation);

    return (
    <div>
        <div>
            <button onClick={replyCallback}> 
                {replyButtonText}
            </button>
          {displayReplyToAll && (
            <button onClick={replyAllCallback}>
              ReplyToAll
            </button>
          )}
        </div>
        <button onClick={discardCallback}>
            Discard
        </button>
    </div>
      
  );
}

export default ReplyFooter;