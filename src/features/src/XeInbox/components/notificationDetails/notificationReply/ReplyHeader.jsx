import React from 'react';

export const ReplyHeader = ({ recipientLastName, recipientFirstName }) => {

    return (
      <span data-component-name="Header" className="reply-header">
        {`Reply To ${recipientFirstName} ${recipientLastName}`}
      </span>
    );
}

export default ReplyHeader;