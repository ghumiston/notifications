import React from 'react';

export const LoadingView = () => (
  <div>
    <span>
      Loading
    </span>
  </div>
);

export default LoadingView;