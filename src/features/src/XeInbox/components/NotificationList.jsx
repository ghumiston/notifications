import React, { useState } from 'react';
import NotificationRenderer from './NotificationRenderer';
import ActionBar from './ActionBar';

// Note: selected multiple notifications that are a set
// of either mixed "unread/read" statuses OR mixed "flagged/unflagged"
// results in the flag and unread actions being disabled (GCH)

export const NotificationList = ({xeAppUserNotifications = [], selectedNotificationId}) => {
    // TODO: revisit prod app to see how this is managed (GCH)
    const [checkedNotifications, setCheckedNotifications] = useState({});
    const shouldDisableActionBar = !Object.values(checkedNotifications).some(isChecked => isChecked);

    return (
        <div style={{width: '300px'}}>
          <ActionBar disabled={shouldDisableActionBar}/>
          {xeAppUserNotifications.map(
              ({XeAppUserNotification}, index) => {
                const notificationId = XeAppUserNotification.NotificationID;
                return (
                  <NotificationRenderer
                    key={`NotificationID-${notificationId}`}
                    XeAppUserNotification={XeAppUserNotification}
                    isSelected={selectedNotificationId === notificationId}
                    isChecked={checkedNotifications[index]}
                    onCheck={(checkStatus) => {
                      setCheckedNotifications({...checkedNotifications, [notificationId]: checkStatus})
                    }}
                  />
                );
              }
            )}
        </div>
    );

}

export default NotificationList;