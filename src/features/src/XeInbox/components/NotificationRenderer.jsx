import React from 'react';
import { Link } from 'react-router-dom';
import './notificationRenderer.css';

export const NotificationRenderer = ({
  XeAppUserNotification, 
  isSelected, 
  isChecked,
  onCheck
}) => {
    const {
      // Attachment,
      Subject,
      NotificationID,
      // PriorityID, 
      SendDate,
      SenderResID,
      DerivedRecipientResID,
      StatusID
    } = XeAppUserNotification;
    // const { PriorityID: PriorityIDString, Name } = PriorityID;

    return (
      <div
        // name={name}
        data-component-name="MessageRenderer"
        className={`notification-item ${isSelected ? 'notif-box-selected' : ''}`}
        // tabIndex={tabIndex}
        // onClick={() => onView(notification)}
      >
        {/* TODO: we should find a way to not hardcode the path here. */}
        <Link to={`/xeInbox/${NotificationID}`}>
          <div className="notification-icon-container">
            <div onClick={(event) => event.stopPropagation()}>
              <input
                type="checkbox"
                name="selectNotification"
                style={{ display: 'inline-block' }}
                checked={isChecked}
                onChange={(data) => {
                  const { target: { checked = false } } = data;
                  onCheck(checked);
                }}
              />
            </div>
            {/* TODO: integrate priority icon (GCH) */}
            {/* <Icon icon={this.toPriorityIcon(PriorityIDString)} /> */}
          </div>
          <div className="message-container">
            <div className="date message-container__date">
              <span>
                {SendDate}
              </span>
            </div>
            {(SenderResID) && (
              <div className="name">
                {SenderResID ? (
                  <span>
                    {`${SenderResID.GivenName} ${SenderResID.FamilyName}`}
                  </span>
                ) : (
                  <div className="recipients-list">
                  {
                    DerivedRecipientResID.map(({ GivenName, FamilyName }, index, array) =>
                      <span className={`name ${StatusID === 'UNREAD' ? 'bold' : ''}`}>
                        {`${GivenName} ${FamilyName}`}
                        {index === array.length - 1 ? '' : ', '}
                      </span>
                    )
                  }
                  </div>
                )}
              </div>
            )}
            <div
              className={`subject ${
                StatusID === 'READ' ? 'subject-read' : ''
              }`}
            >
              <span>{Subject}</span>
            </div>
          </div>
          <div className="vertical-flex-container">
            {/* TODO: integrate flagged mail icon (GCH) */}
            {/* {showFlag && IsFlagged && <Icon icon={FLAGGED_MAIL} />} */}
            {/* TODO: add attachment data to mock this out, integrate attachment icon (GCH) */}
            {/* {
              Array.isArray(Attachment) &&
              !!Attachment.length && (
                <img
                  src={ATTACH_FILE}
                  className="attachment-indicator flex-1 align-items-end"
                />
              )
            } */}
          </div>
        </Link>
      </div>
    );

};

export default NotificationRenderer;
