import { useQuery } from 'react-query';
import { toItemDetailsQuery, toInboxQuery, toGetPatientQuery } from './queries';
import { useParams } from 'react-router-dom';

export const useItemDetailsQuery = (selectedNotificationId) => {
  const { 
    data: { 
      results: { 
        XeAppUserNotification: currentXeAppUserNotification
      } = {} 
    } = {} 
  } = useQuery(['itemDetails', selectedNotificationId], toItemDetailsQuery(selectedNotificationId),
    {
      //disabling refresh for now, we can add back later
      //refetchInterval: 5000,
      refetchIntervalInBackground: true,
      enabled: selectedNotificationId !== undefined
    }
  )
  
  return currentXeAppUserNotification;
};

export const useInboxQuery = () => {
  const { data = {}, isLoading, isError, error } = useQuery('Inbox', toInboxQuery(),
    {
      //refetchInterval: 5000,
      refetchIntervalInBackground: true
    }
  );
  const { results: XeAppUserNotifications } = data;

  return {
    XeAppUserNotifications,
    isLoading, 
    isError, 
    error
  };
};

export const useParseIdParam = () => {
  const { id: idString } = useParams();
  const id = !!idString ? parseInt(idString) : undefined;
  return id;
};

export const useGetPatientQuery = (ipid) => {
  const { data = [] } = useQuery(['getPatient', ipid], toGetPatientQuery(ipid));
  const { results = [] } = data;
  return results[0];
}

// note: depending on source (inbox/outbox/deleted), DataBlob may be in a different location
const parseBlobForIPID = ({DataBlob}) => {
  
  if (!DataBlob) {
    return undefined;
  }

  const xml = new DOMParser().parseFromString(DataBlob, 'application/xml');
  const IPID = xml.querySelector('IPID');

  return IPID && IPID.textContent;
};

export const useGetAssociatedPatient = (xeAppUserNotification = {}) => {
  const associatedPatientIPID = parseBlobForIPID(xeAppUserNotification);
  const associatedPatient = useGetPatientQuery(associatedPatientIPID);
  return associatedPatient;
}