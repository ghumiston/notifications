export const visitFormatter = ({ VisitID, PatientTypeID = {} }) =>
  PatientTypeID.Name ? `${VisitID} (${PatientTypeID.Name})` : VisitID;

//TODO:  fill this in when we move over everything over to the thrasys project
export const patientDetailsFormatter = (patient) => {
  const { FamilyName, GivenName, SexID, DoB, DateOfDeath } = patient;
  return `${GivenName} ${FamilyName} (${SexID}), ${DoB}, ${DateOfDeath}`;
}