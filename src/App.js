import './App.css';
// import XeNotifications from './features/src/XeNotifications';
import XeInbox from './features/src/XeInbox';
// import XeSent from './features/src/XeSent';
// import XeArchived from './features/src/XeArchived';
import { QueryClient, QueryClientProvider } from 'react-query'
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";

const queryClient = new QueryClient();

function App() {
  return (
    <div className="App">
      <QueryClientProvider client={queryClient}>
        
        <Router>
            <Routes>
              <Route path='/'>
                <h1>Main</h1>
              </Route>
              <Route path='/xeInbox/:id' element={<XeInbox/>}/>
              <Route path='/xeInbox' element={<XeInbox/>}/>
            </Routes>
        </Router>        

      </QueryClientProvider>
    </div>
  );
}

export default App;
